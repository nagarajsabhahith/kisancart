<?php defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>FarmBazaar</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="" name="keywords">
	<meta content="" name="description">

<!--	<link href="--><?php //echo asset_url();?><!--img/favicon.png" rel="icon">-->
<!--	<link href="--><?php //echo asset_url();?><!--img/apple-touch-icon.png" rel="apple-touch-icon">-->

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

	<link href="<?php echo asset_url();?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<link href="<?php echo asset_url();?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo asset_url();?>plugins/animate/animate.min.css" rel="stylesheet">
	<link href="<?php echo asset_url();?>plugins/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="<?php echo asset_url();?>plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
	<link href="<?php echo asset_url();?>plugins/lightbox/css/lightbox.min.css" rel="stylesheet">

	<link href="<?php echo asset_url();?>css/style.css" rel="stylesheet">

</head>

<body>

<header id="header">
	<div class="container-fluid">

		<div id="logo" class="pull-left">
<!--			<img src="--><?php //echo asset_url();?><!--img/farm.png" alt="" class="img-fluid">-->
			<h1><a href="<?php echo site_url();?>" class="scrollto">FarmBazaar</a></h1>

		</div>

		<nav id="nav-menu-container">
			<ul class="nav-menu">
				<li class="menu-active"><a href="#intro">Home</a></li>
				<li><a href="#services">Services</a></li>
                <li><a href="#about">About Us</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
		</nav>
	</div>
</header>

<section id="intro">
	<div class="intro-container">
		<div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

			<ol class="carousel-indicators"></ol>

			<div class="carousel-inner" role="listbox">

				<div class="carousel-item active" style="background-image: url('<?php echo asset_url();?>img/intro-carousel/1.jpg');">
					<div class="carousel-container">
						<div class="carousel-content">
							<h2> Best price for Farmers </h2>
							<!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
						</div>
					</div>
				</div>

				<div class="carousel-item" style="background-image: url('<?php echo asset_url();?>img/intro-carousel/2.jpg');">
					<div class="carousel-container">
						<div class="carousel-content">
							<h2>Better margins for Retailers </h2>
							<!-- <p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut.</p> -->

						</div>
					</div>
				</div>

				<div class="carousel-item" style="background-image: url('<?php echo asset_url();?>img/intro-carousel/4.jpg');">
					<div class="carousel-container">
						<div class="carousel-content">
							<h2>Fresh fruits & vegetables to consumers </h2>
							<!-- <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum.</p> -->

						</div>
					</div>
				</div>
			</div>

			<a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>

			<a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>

		</div>
	</div>
</section>

<main id="main">

    <section id="services">
        <div class="container">

            <header class="section-header wow fadeInUp">
                <h3>Services</h3>
                <p></p>
            </header>

            <div class="row">

                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
                    <h4 class="title"><a href="">Retail Outlets</a></h4>
                    <p class="description">We supply fresh fruits & vegetables procured directly from farm to retail outlets who order directly using our app</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
                    <h4 class="title"><a href="">Super Markets</a></h4>
                    <p class="description">We do supply and work on shop-in-shop model with Super markets to enhance F&V experience to end consumers ata</p>
                </div>
                <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
                    <div class="icon"><i class="ion-ios-paper-outline"></i></div>
                    <h4 class="title"><a href="">Companies</a></h4>
                    <p class="description">We take bulk orders and supply according to the requirement to E-commerce companies who directly do home delivery</p>
                </div>
            </div>

        </div>
    </section>

	<section id="about">
		<div class="container">

			<header class="section-header">
				<h3>About Us</h3>
				<p>Currently we deliver to most of the premium locations in Bengaluru like Koramangala, Indiranagar, HSR Layout, JP Nagar, Jayanagar, BTM Layout etc.. </p>
			</header>

			<div class="row about-cols">

				<div class="col-md-4 wow fadeInUp">
					<div class="about-col">
						<div class="img">
							<img src="<?php echo asset_url();?>img/about-mission.jpg" alt="" class="img-fluid">
							<div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
						</div>
						<h2 class="title"><a href="#">Our Mission</a></h2>
						<p>
							Deliver fresh fruits & vegetables directly from farms using advanced farming and logistics techniques to enhance the entire ecosystem.
						</p>
					</div>
				</div>

				<div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
					<div class="about-col">
						<div class="img">
							<img src="<?php echo asset_url();?>img/about-plan.jpg" alt="" class="img-fluid">
							<div class="icon"><i class="ion-ios-list-outline"></i></div>
						</div>
						<h2 class="title"><a href="#">Our Plan</a></h2>
						<p>
							Bring all Retail outlets into single chain,To build a reliable retail chain and enhance consumer experience
						</p>
					</div>
				</div>

				<div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
					<div class="about-col">
						<div class="img">
							<img src="<?php echo asset_url();?>img/about-vision.jpg" alt="" class="img-fluid">
							<div class="icon"><i class="ion-ios-eye-outline"></i></div>
						</div>
						<h2 class="title"><a href="#">Our Vision</a></h2>
						<p>
							To be one stop solution for the whole supply chain right from farming techniques till the produce reaches end consumer
						</p>
					</div>
				</div>

			</div>

		</div>
	</section>



<!--	 <section id="clients" class="light-bg wow fadeIn hidden">-->
<!--		<div class="container">-->
<!--            <header class="section-header">-->
<!--                <h3>Our Investors</h3>-->
<!--            </header>-->
<!---->
<!--            <div class="owl-carousel clients-carousel">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-1.png" alt="">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-2.png" alt="">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-3.png" alt="">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-4.png" alt="">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-5.png" alt="">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-6.png" alt="">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-7.png" alt="">-->
<!--                <img src="--><?php //echo asset_url();?><!--img/clients/client-8.png" alt="">-->
<!--            </div>-->
<!--		</div>-->
<!--	</section>-->

	<section id="facts"  class="wow fadeIn dark">
		<div class="container">

			<header class="section-header">
				<h3 class="">Gallery</h3>
				<p class=""></p>
			</header>


			<div class="facts-img">
                <div class="owl-carousel gallery-carousel">
                    <img src="<?php echo asset_url();?>img/intro-carousel/1.jpg" alt="">
                    <img src="<?php echo asset_url();?>img/intro-carousel/2.jpg" alt="">
                    <img src="<?php echo asset_url();?>img/intro-carousel/3.jpg" alt="">
                    <img src="<?php echo asset_url();?>img/intro-carousel/4.jpg" alt="">
                </div>
			</div>

		</div>
	</section>

	<section id="contact" class="light-bg wow fadeInUp">
		<div class="container">

			<div class="section-header">
				<h3>Contact Us</h3>
				<p>Feel free to contact us using any mode mentioned below</p>
			</div>

			<div class="row contact-info">

				<div class="col-md-4">
					<div class="contact-address">
						<i class="ion-ios-location-outline"></i>
						<h3>Address</h3>
						<address>Koramangala, Bengaluru</address>
					</div>
				</div>

				<div class="col-md-4">
					<div class="contact-phone">
						<i class="ion-ios-telephone-outline"></i>
						<h3>Phone Number</h3>
						<p><a href="tel:+155895548855">+91 8105768628</a></p>
					</div>
				</div>

				<div class="col-md-4">
					<div class="contact-email">
						<i class="ion-ios-email-outline"></i>
						<h3>Email</h3>
						<p><a href="mailto:care@farmbazaar.in">care@farmbazaar.in</a></p>
					</div>
				</div>

			</div>

		</div>
	</section>
    <section id="contact" class="white-bg wow fadeInUp">
        <div class="container">
            <div class="section-header">
                <h3>Reach Us</h3>
            </div>

            <div class="form">
                <div id="sendmessage">Your message has been sent. Thank you!</div>
                <div id="errormessage"></div>
                <form action="" method="post" role="form" class="contactForm">
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        <div class="validation"></div>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        <div class="validation"></div>
                    </div>
                    <div class="text-center"><button type="submit">Send Message</button></div>
                </form>
            </div>
        </div>
    </section>

</main>

<footer id="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row">

				<div class="col-lg-4 col-md-6 footer-info">
					<h3>Farm Bazaar</h3>
					<p>One stop solution for all the Fruits & Vegetable needs for retailers across Bengaluru. We procure fresh fruits & vegetables directly from farms and supply to retailers with in no time. That way we preserve freshness of fruits & vegetables we supply. </p>
				</div>

				<div class="col-lg-4 col-md-6 footer-links">
					<h4>Useful Links</h4>
					<ul>
						<li><i class="ion-ios-arrow-right"></i> <a href="#">Home</a></li>
						<li><i class="ion-ios-arrow-right"></i> <a href="#">About us</a></li>
						<li><i class="ion-ios-arrow-right"></i> <a href="#">Services</a></li>
						<li><i class="ion-ios-arrow-right"></i> <a href="#">Terms of service</a></li>
						<li><i class="ion-ios-arrow-right"></i> <a href="#">Privacy policy</a></li>
					</ul>
				</div>

				<div class="col-lg-4 col-md-6 footer-contact">
					<h4>Contact Us</h4>
					<p>
                        91 Springboard <br>
                        Next to Forum mall<br>
                        Koramangala <br>
						<strong>Phone:</strong> +91 8105768628<br>
						<strong>Email:</strong> care@farmbazaar.in<br>
					</p>

					<div class="social-links">
						<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
						<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
						<a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
						<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
						<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
					</div>

				</div>

			</div>
		</div>
	</div>

	<div class="container">
		<div class="copyright">
			&copy; Copyright <strong>Farm Bazaar</strong>. All Rights Reserved
		</div>
	</div>
</footer>

<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>


<script src="<?php echo asset_url();?>plugins/jquery/jquery.min.js"></script>
<script src="<?php echo asset_url();?>plugins/jquery/jquery-migrate.min.js"></script>
<script src="<?php echo asset_url();?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo asset_url();?>plugins/easing/easing.min.js"></script>
<script src="<?php echo asset_url();?>plugins/superfish/hoverIntent.js"></script>
<script src="<?php echo asset_url();?>plugins/superfish/superfish.min.js"></script>
<script src="<?php echo asset_url();?>plugins/wow/wow.min.js"></script>
<script src="<?php echo asset_url();?>plugins/waypoints/waypoints.min.js"></script>
<script src="<?php echo asset_url();?>plugins/counterup/counterup.min.js"></script>
<script src="<?php echo asset_url();?>plugins/owlcarousel/owl.carousel.min.js"></script>
<script src="<?php echo asset_url();?>plugins/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo asset_url();?>plugins/lightbox/js/lightbox.min.js"></script>
<script src="<?php echo asset_url();?>plugins/touchSwipe/jquery.touchSwipe.min.js"></script>

<!--<script src="--><?php //echo asset_url();?><!--js/contactform.js"></script>-->

<script src="<?php echo asset_url();?>js/main.js"></script>

</body>
</html>

